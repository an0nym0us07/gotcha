<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author Lukas Jansen
 */
class User extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('upload');
        $this->load->library('session');
    }
    
    public function index()
    {
        $inhoud['titel'] = 'Gotcha';
        $inhoud['geenRand'] = true;      // geen extra rand rond hoofdmenu
        if(!$this->user_model->slachtofferIsSet(1))
        {
            $partials = array('hoofding' => 'main_header',
            'inhoud' => 'login',
            'registratie' => 'registratie');
        }
        else
        {
            $partials = array('hoofding' => 'main_header',
            'inhoud' => 'login');
        }
        $this->template->load('main_master', $partials, $inhoud);
    }

    public function registerUser()
    {
        if($this->input->post('wachtwoord') == $this->input->post('herhaalWachtwoord'))
        {
            $user = array(
                'gebruikersnaam' => $this->input->post('gebruikersnaam'),
                'email' => $this->input->post('email'),
                'wachtwoord' => password_hash($this->input->post('wachtwoord'), PASSWORD_DEFAULT),
                'foto' => 'no_image.png'
            );
            $gebruikersnaam_check = $this->user_model->gebruikersnaamCheck($user['gebruikersnaam']);

            $target_dir = "/var/www/html/assets/images/";
            $target_file = $target_dir . basename($_FILES['userfile']['name']);
            if(!($_FILES['userfile']['name'] == "" && $_FILES['userfile']['size'] == 0))
            {
                $user['foto'] = basename($_FILES['userfile']['name']);
            }
            if(move_uploaded_file($_FILES['userfile']['tmp_name'], $target_file))
            {

                $this->session->set_flashdata("success_msg", "Registratie voltooid");
            }       
            if($gebruikersnaam_check)
            {
                $this->user_model->registerUser($user);
                $this->session->set_flashdata('success_msg', 'Registratie voltooid');
                redirect('user');
            }
            else
            {
                $this->session->set_flashdata('error_msg', 'Gebruikersnaam is al in gebruik.');
                redirect('user');
            }
        }
        else
        {
            $this->session->set_flashdata('error_msg', 'Wachtwoorden komen niet overeen');
            redirect('user');
        }
    }
    
    public function loginUser()
    {
        $user_login = array(
            'gebruikersnaam' => $this->input->post('gebruikersnaam'),
            'wachtwoord' => $this->input->post('wachtwoord')
            );
        
        $data = $this->user_model->loginUser($user_login['gebruikersnaam'], $user_login['wachtwoord']);
        if($data)
        {
            $this->session->set_userdata('id', $data['id']);
            $this->session->set_userdata('gebruikersnaam', $data['gebruikersnaam']);
            $this->session->set_userdata('email', $data['email']);
            $this->session->set_userdata('foto', $data['foto']);
            $this->session->set_userdata('slachtofferId', $data['slachtofferId']);
            $this->session->set_userdata('aantalKills', $data['aantalKills']);
            $this->session->set_userdata('levend', $data['levend']);
            $this->session->set_userdata('regelsGoedgekeurd', $data['regelsGoedgekeurd']);
            
            $dataSlachtoffer = $this->user_model->getSlachtoffer($_SESSION['slachtofferId']);
            $this->session->set_userdata('slachtofferGebruikersnaam', $dataSlachtoffer['gebruikersnaam']);
            $this->session->set_userdata('slachtofferFoto', $dataSlachtoffer['foto']);
            $this->session->set_userdata('slachtoffersSlachtofferId', $dataSlachtoffer['slachtofferId']);
            $this->user_model->setUniekeCode($_SESSION['id']);
            
            $inhoud['id'] = $_SESSION['id'];
            $inhoud['gebruikersnaam'] = $_SESSION['gebruikersnaam'];
            $inhoud['email'] = $_SESSION['email'];
            $inhoud['foto'] = $_SESSION['foto'];
            $inhoud['slachtofferGebruikersnaam'] = $_SESSION['slachtofferGebruikersnaam'];
            
            if(!$_SESSION['regelsGoedgekeurd'])
            {
                $data['titel'] = "Regels Gotcha";
                $data['geenRand'] = true;
        
                $partials = array('hoofding' => 'main_header',
                    'inhoud' => 'regels');
                $this->template->load('main_master', $partials, $data);
            }
            else
            {
                if($_SESSION['id']==$dataSlachtoffer['slachtofferId'])
                {
                    $data['titel'] = 'Gotcha gewonnen';
                    $data['geenRand'] = true;      // geen extra rand rond hoofdmenu

                    $partials = array('hoofding' => 'main_header',
                        'inhoud' => 'gewonnen');
                    $this->template->load('main_master', $partials, $data);
                }
                else
                {
                    $inhoud['titel'] = 'Gotcha';
                    $inhoud['geenRand'] = true;      // geen extra rand rond hoofdmenu

                    $partials = array('hoofding' => 'main_header',
                        'inhoud' => 'main_menu');
                    $this->template->load('main_master', $partials, $inhoud);
                }
                
            }
        }
        else if($this->user_model->gebruikersnaamCheck($user_login['gebruikersnaam']))
        {
            $this->session->set_flashdata('error_msg_login', 'Gegevens niet correct, probeer later opnieuw.');
            redirect('user');
        }
        else
        {
            $this->session->set_flashdata('error_msg_login', 'Gegevens niet correct, probeer later opnieuw.');
            redirect('user');
        }
    }
    
    public function wachtwoordVergeten()
    {
        $inhoud['titel'] = 'Gotcha';
        $inhoud['geenRand'] = true;      // geen extra rand rond hoofdmenu
        $partials = array('hoofding' => 'main_header',
          'inhoud' => 'wachtwoordVergeten');
        $this->template->load('main_master', $partials, $inhoud);
    }
    
    public function userLogout()
    {
        $this->session->sess_destroy();
        redirect('user', 'refresh');
    }
    
    public function wijzigWachtwoord()
    {
        if($this->input->post('wachtwoord') == $this->input->post('herhaalWachtwoord'))
        {
            $user = array(
                'gebruikersnaam' => $this->input->post('gebruikersnaam'),
                'email' => $this->input->post('email'),
                'wachtwoord' => password_hash($this->input->post('wachtwoord'), PASSWORD_DEFAULT),
            );
            
            if(!$this->user_model->gebruikersnaamCheck($user['gebruikersnaam']))
            {
                $this->user_model->wijzigWachtwoord($user['gebruikersnaam'], $user['wachtwoord']);
            
                $from = 'jansen.lukas1998@gmail.com';
                $to = $user['email'];
                $subject = 'Gotcha!';
                $header = 'From: jansen.lukas1998@gmail.com';

                $body = 'Hallo ' . $user['gebruikersnaam'] . "!\r\n" . 'Je wachtwoord is gewijzigd!' . 'surf snel naar http://gotcha.ddnsking.com voor meer informatie!';

                mail($to, $subject, $body, $header);
                redirect('user');
            }
            else
            {
                $this->session->set_flashdata('error_msg', 'Gebruikersnaam bestaat niet');
                redirect('user/wijzigWachtwoord');
            }
        }
        else
        {
            $this->session->set_flashdata('error_msg', 'Wachtwoorden komen niet overeen');
            redirect('user/wijzigWachtwoord');
        }
    }
}

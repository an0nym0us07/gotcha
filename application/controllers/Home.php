<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author Lukas Jansen
 */
class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('moord_model');
    }

    public function index() {
        $data['titel'] = 'Gotcha';
        $data['geenRand'] = true;      // geen extra rand rond hoofdmenu

        $partials = array('hoofding' => 'main_header',
            'inhoud' => 'main_menu');
        $this->template->load('main_master', $partials, $data);
    }

    public function bepaalSlachtoffer() {
        $spelers = $this->user_model->getSpelers();
        $spelers = array_column($spelers, 'id');
        shuffle($spelers);
        for ($i = 0; $i < count($spelers); $i++) {
            if ($i == count($spelers) - 1) {
                $this->user_model->setSlachtoffer($spelers[$i], $spelers[0]);
            } else {
                $this->user_model->setSlachtoffer($spelers[$i], $spelers[$i + 1]);
            }
        }
        $spelers = $this->user_model->getAllSpelers();
        foreach ($spelers as $speler)
        {
            $slachtoffer = $this->user_model->getSlachtoffer($speler->slachtofferId);
            $from = 'jansen.lukas1998@gmail.com';
            $to = $speler->email;
            $subject = 'Gotcha!';
            $header = 'From: jansen.lukas1998@gmail.com';
            
            $body = 'Hallo ' . $speler->gebruikersnaam . "!\r\n" . 'De slachtoffers zijn bekend!' . "\r\n" . 'Jouw eerste slachtoffer is: ' . $slachtoffer['gebruikersnaam'] . "\r\n" . 'surf snel naar http://gotcha.ddnsking.com voor meer informatie!';
            
            mail($to, $subject, $body, $header);
        }
        redirect('user');
    }

    public function benVermoord() {
        $code = $this->user_model->getUniekeCode($_SESSION['id']);
        $this->session->set_flashdata('vermoord_msg', 'unieke code: ' . $code);
        redirect('home');
    }

    public function hebVermoord($code = "", $scan = false) {
        if(!$scan)
        {
            if ($this->input->post('uniekeCode') == $this->user_model->getUniekeCode($_SESSION['slachtofferId'])) {
                $this->user_model->vermoord($_SESSION['slachtofferId']);
                $this->user_model->setSlachtoffer($_SESSION['id'], $_SESSION['slachtoffersSlachtofferId']);
                $dataSlachtoffer = $this->user_model->getSlachtoffer($_SESSION['slachtoffersSlachtofferId']);
                $this->session->set_userdata('slachtofferGebruikersnaam', $dataSlachtoffer['gebruikersnaam']);
                $this->session->set_userdata('slachtofferFoto', $dataSlachtoffer['foto']);
                $this->session->set_userdata('slachtoffersSlachtofferId', $dataSlachtoffer['slachtofferId']);
                $aantalKills = $this->user_model->getAantalKills($_SESSION['id']);
                $aantalKills++;
                $_SESSION['aantalKills'] = $aantalKills;
                $this->user_model->setAantalKills($_SESSION['id'], $aantalKills);
                $moord = array(
                    'spelerId' => $_SESSION['id'],
                    'slachtofferId' => $_SESSION['slachtofferId'],
                    'omschrijving' => $this->input->post('omschrijving'),
                    'tijdstip' => date("Y-m-d H:i:s")
                );
                $this->moord_model->setOmschrijving($moord);
                if($_SESSION['id']==$dataSlachtoffer['id'])
                {
                    $data['titel'] = 'Gotcha gewonnen';
                    $data['geenRand'] = true;      // geen extra rand rond hoofdmenu

                    $partials = array('hoofding' => 'main_header',
                        'inhoud' => 'gewonnen');
                    $this->template->load('main_master', $partials, $data);
                }
                else
                {
                    redirect('home', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error_vermoord_msg', 'Code onjuist');
                redirect('home');
            }
        }
        else
        {
            $data['code'] = $code;
            $data['titel'] = "Omschrijving moord";
            $data['geenRand'] = true;      // geen extra rand rond hoofdmenu

            $partials = array('hoofding' => 'main_header',
                'inhoud' => 'omschrijving');
            $this->template->load('main_master', $partials, $data);
        }
    }
    
    public function setOmschrijving()
    {
        $user_login = array(
            'gebruikersnaam' => $this->input->post('gebruikersnaam'),
            'wachtwoord' => $this->input->post('wachtwoord')
            );
        
        $data = $this->user_model->loginUser($user_login['gebruikersnaam'], $user_login['wachtwoord']);
        if($data)
        {
            $this->session->set_userdata('id', $data['id']);
            $this->session->set_userdata('gebruikersnaam', $data['gebruikersnaam']);
            $this->session->set_userdata('email', $data['email']);
            $this->session->set_userdata('foto', $data['foto']);
            $this->session->set_userdata('slachtofferId', $data['slachtofferId']);
            $this->session->set_userdata('aantalKills', $data['aantalKills']);
            $this->session->set_userdata('levend', $data['levend']);
            $this->session->set_userdata('regelsGoedgekeurd', $data['regelsGoedgekeurd']);
            
            $dataSlachtoffer = $this->user_model->getSlachtoffer($_SESSION['slachtofferId']);
            $this->session->set_userdata('slachtofferGebruikersnaam', $dataSlachtoffer['gebruikersnaam']);
            $this->session->set_userdata('slachtofferFoto', $dataSlachtoffer['foto']);
            $this->session->set_userdata('slachtoffersSlachtofferId', $dataSlachtoffer['slachtofferId']);
            $this->user_model->setUniekeCode($_SESSION['id']);
            
            $inhoud['id'] = $_SESSION['id'];
            $inhoud['gebruikersnaam'] = $_SESSION['gebruikersnaam'];
            $inhoud['email'] = $_SESSION['email'];
            $inhoud['foto'] = $_SESSION['foto'];
            $inhoud['slachtofferGebruikersnaam'] = $_SESSION['slachtofferGebruikersnaam'];
            
            if($this->input->post('code') == $this->user_model->getUniekeCode($_SESSION['slachtofferId']))
            {
                $this->user_model->vermoord($_SESSION['slachtofferId']);
                $this->user_model->setSlachtoffer($_SESSION['id'], $_SESSION['slachtoffersSlachtofferId']);
                $moord = array(
                    'spelerId' => $_SESSION['id'],
                    'slachtofferId' => $_SESSION['slachtofferId'],
                    'omschrijving' => $this->input->post('omschrijving'),
                    'tijdstip' => date("Y-m-d H:i:s")
                );
                $this->moord_model->setOmschrijving($moord);
                $aantalKills = $this->user_model->getAantalKills($_SESSION['id']);
                $aantalKills++;
                $_SESSION['aantalKills'] = $aantalKills;
                $this->user_model->setAantalKills($_SESSION['id'], $aantalKills);
                if($_SESSION['id']==$dataSlachtoffer['slachtofferId'])
                {
                    $data['titel'] = 'Gotcha gewonnen';
                    $data['geenRand'] = true;      // geen extra rand rond hoofdmenu

                    $partials = array('hoofding' => 'main_header',
                        'inhoud' => 'gewonnen');
                    $this->template->load('main_master', $partials, $data);
                }
                else
                {
                    $slachtoffer = $this->user_model->getSlachtoffer($speler->slachtofferId);
                    $from = 'jansen.lukas1998@gmail.com';
                    $to = $_SESSION['email'];
                    $subject = 'Gotcha!';
                    $header = 'From: jansen.lukas1998@gmail.com';
            
                    $body = 'Hallo ' . $_SESSION['gebruikersnaam'] . "!\r\n" . 'Jouw nieuw slachtoffer is: ' . $slachtoffer['gebruikersnaam'] . "\r\n" . 'surf snel naar http://gotcha.ddnsking.com voor meer informatie!';
            
                    mail($to, $subject, $body, $header);
                    $this->session->sess_destroy();
                    redirect('user', 'refresh');
                }
                
            }
            else
            {
                $this->session->set_flashdata('error_vermoord_msg', 'Onjuist slachtoffer!');
                redirect('home/hebVermoord/' . $this->input->post('code') . '/true');
            }
        } else
        {
            $this->session->set_flashdata('error_vermoord_msg', 'Onjuiste inloggegevens!');
            redirect('home/hebVermoord/' . $this->input->post('code') . '/true');
        }
    }

    public function toonSpelers() {
        $spelers = $this->user_model->getAllSpelers();
        $data['spelers'] = $spelers;

        $data['titel'] = "Gotcha";
        $data['geenRand'] = true;      // geen extra rand rond hoofdmenu

        $partials = array('hoofding' => 'main_header',
            'inhoud' => 'spelers');
        $this->template->load('main_master', $partials, $data);
    }

    public function toonRegels()
    {
        $data['titel'] = "Regels Gotcha";
        $data['geenRand'] = true;
        
        $partials = array('hoofding' => 'main_header',
            'inhoud' => 'regels');
        $this->template->load('main_master', $partials, $data);
    }
    
    public function keurGoed()
    {
        $this->user_model->keurGoed($_SESSION['id']);
        $this->session->set_userdata('regelsGoedgekeurd', 1);
        $inhoud['id'] = $_SESSION['id'];
        $inhoud['gebruikersnaam'] = $_SESSION['gebruikersnaam'];
        $inhoud['email'] = $_SESSION['email'];
        $inhoud['foto'] = $_SESSION['foto'];
        $inhoud['slachtofferGebruikersnaam'] = $_SESSION['slachtofferGebruikersnaam'];
    
        $inhoud['titel'] = 'Gotcha';
        $inhoud['geenRand'] = true;      // geen extra rand rond hoofdmenu

        $partials = array('hoofding' => 'main_header',
            'inhoud' => 'main_menu');
        $this->template->load('main_master', $partials, $inhoud);
    }
    
    public function wijzigPagina()
    {
        $inhoud['id'] = $_SESSION['id'];
        $inhoud['gebruikersnaam'] = $_SESSION['gebruikersnaam'];
        $inhoud['email'] = $_SESSION['email'];
        $inhoud['foto'] = $_SESSION['foto'];
        
        $inhoud['titel'] = 'Gotcha';
        $inhoud['geenRand'] = true;
        
        $partials = array('hoofding' => 'main_header',
            'inhoud' => 'wijzigGegevens');
        $this->template->load('main_master', $partials, $inhoud);
    }
    
    public function wijzigGegevens()
    {
        $checked = $this->input->post('cbFoto');
        if($checked != 'behoud')
        {
            $user = array(
                'id' => $_SESSION['id'],
                'gebruikersnaam' => $this->input->post('gebruikersnaam'),
                'email' => $this->input->post('email'),
                'foto' => 'no_image.png'
            );
            $target_dir = "/var/www/html/assets/images/";
            $target_file = $target_dir . basename($_FILES['gebruikerfoto']['name']);
            if(!($_FILES['gebruikerfoto']['name'] == "" && $_FILES['gebruikerfoto']['size'] == 0))
            {
                $user['foto'] = basename($_FILES['gebruikerfoto']['name']);
            }
            if(move_uploaded_file($_FILES['gebruikerfoto']['tmp_name'], $target_file))
            {
                $this->user_model->wijzigUser($user);
                $_SESSION['gebruikersnaam'] = $user['gebruikersnaam'];
                $_SESSION['email'] = $user['email'];
                $_SESSION['foto'] = $user['foto'];
            }       
            $gebruikersnaam_check = $this->user_model->gebruikersnaamCheck($user['gebruikersnaam']);
            if($gebruikersnaam_check || $user['gebruikersnaam'] == $_SESSION['gebruikersnaam'])
            {
                $this->user_model->wijzigUser($user);
                $_SESSION['gebruikersnaam'] = $user['gebruikersnaam'];
                $_SESSION['email'] = $user['email'];
                $_SESSION['foto'] = $user['foto'];
                $data['titel'] = 'Gotcha';
                $data['geenRand'] = true;      // geen extra rand rond hoofdmenu

                $partials = array('hoofding' => 'main_header',
                    'inhoud' => 'main_menu');
                $this->template->load('main_master', $partials, $data);
            }
            else
            {
                $this->session->set_flashdata('error_msg', 'Gebruikersnaam is al in gebruik.');
                redirect('home/wijzigGegevens');
            }
        }
        else
        {
            $user = array(
                'id' => $_SESSION['id'],
                'gebruikersnaam' => $this->input->post('gebruikersnaam'),
                'email' => $this->input->post('email'),
                'foto' => $_SESSION['foto']
            );
            $gebruikersnaam_check = $this->user_model->gebruikersnaamCheck($user['gebruikersnaam']);
            if($gebruikersnaam_check || $user['gebruikersnaam'] == $_SESSION['gebruikersnaam'])
            {
                $this->user_model->wijzigUser($user);
                $_SESSION['gebruikersnaam'] = $user['gebruikersnaam'];
                $_SESSION['email'] = $user['email'];
                $_SESSION['foto'] = $user['foto'];
                $data['titel'] = 'Gotcha';
                $data['geenRand'] = true;      // geen extra rand rond hoofdmenu

                $partials = array('hoofding' => 'main_header',
                    'inhoud' => 'main_menu');
                $this->template->load('main_master', $partials, $data);
            }
            else
            {
                $this->session->set_flashdata('error_msg', 'Gebruikersnaam is al in gebruik.');
                redirect('home/wijzigGegevens');
            }
        }
        
    }
}

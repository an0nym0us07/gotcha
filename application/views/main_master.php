<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Gotcha">
        <meta name="author" content="Lukas Jansen">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <title><?php echo $titel;?></title>

        <!-- Bootstrap Core CSS -->
        <?php echo pasStylesheetAan("bootstrap.css"); ?>
        <!-- Custom CSS -->
        <?php echo pasStylesheetAan("heroic-features.css"); ?>
        <!-- Buttons CSS -->
        <?php echo pasStylesheetAan("buttons.css"); ?>
        
        <?php echo haalJavascriptOp("jquery-3.1.0.min.js"); ?>
        <?php echo haalJavascriptOp("bootstrap.js"); ?>

        <script type="text/javascript">
            var site_url = '<?php echo site_url(); ?>';
            var base_url = '<?php echo base_url(); ?>';
        </script>

    </head>

    <body>
        <!-- Page Content -->
        <div class="container">

            <!-- Jumbotron Header -->
            <header class="jumbotron hero-spacer text-center">
                <?php echo $hoofding; ?>
            </header
            <hr>
            <!-- Page Features -->
            <?php if (isset($geenRand)) { ?>
                <div class="row justify-content-between text-center">
                    <?php 
                        echo $inhoud;
                        if(isset($registratie))
                        {
                            echo $registratie;
                        } 
                    ?>
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="col-lg-12 hero-feature">
                        <div class="thumbnail" style="padding: 20px">
                            <div class="caption">
                                <p>
                                    <?php 
                                        echo $inhoud;
                                        if(isset($registratie))
                                        {
                                            echo $registratie;
                                        }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>        
            <?php } ?>
            <!-- /.row -->

            <hr>

        </div>
        <!-- /.container -->

    </body>

</html>

<div class="row">
    <div class="col-md-12 col-sm-12 hero-feature">
        <div class="thumbnail" style="padding: 20px">
            <h3>
                Jouw target is:
                <div>
                    <?php
                        echo $_SESSION['slachtofferGebruikersnaam'] . '</br>' . toonAfbeelding($_SESSION['slachtofferFoto'], 'width = 200px');
                    ?>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 hero-feature">
                        <?php
                            echo '<a class="" id="benVermoord" href="' . base_url('index.php/home/benVermoord') . '">';
                        ?>
                        <button type="button" class="btn btn-primary btn-danger">Ik ben vermoord!</button></a>
                        <?php
                        $vermoord_msg = $this->session->flashdata('vermoord_msg');
                        $src = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=http%3A%2F%2Fgotcha.ddnsking.com/index.php/home/hebVermoord/" . $this->user_model->getUniekeCode($_SESSION['id']) . '/true';
                        if ($vermoord_msg) {
                            echo '<div class="alert alert-danger" role="danger">' . $vermoord_msg . '</div>';
                            echo '<img src="' . $src . '" title="ik ben vermoord" />';
                        }
                        ?>
                    </div>
                    <div class="col-md-6 col-sm-12 hero-feature">
                        <button type="button" class="btn btn-primary btn-success" id="hebVermoord">Ik heb iemand vermoord!</button>
                        <script type="text/javascript">
                            $(document).ready(function(){
                               $("#hebVermoord").click(function()
                               {
                                  $("form").removeClass("invisible"); 
                               });
                            });
                        </script>
                        <form class="invisible" method="post" action="<?php echo base_url('index.php/home/hebVermoord'); ?>">
                            <div class="row">
                                <input type="text" name="uniekeCode" placeholder="unieke code"/>
                            </div>
                            <div class="row">
                                <textarea type=text" name="omschrijving" rows="5" placeholder="Beschrijving moord"></textarea>
                            </div>
                            <input type="submit" name="submit" value="Vermoord <?php echo $_SESSION['slachtofferGebruikersnaam'];?>!">
                        </form>
                        <?php
                            $error_vermoord_msg = $this->session->flashdata('error_vermoord_msg');
                            if($error_vermoord_msg)
                            {
                                echo '<div class="alert alert-danger" role="danger">' . $error_vermoord_msg . '</div>';
                            }
                        ?>
                    </div>
                </div>
            </h3>
        </div>
    </div> 
</div>

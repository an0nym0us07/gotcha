<div class="col-md-6 col-sm-12 hero-feature">
    <div class="thumbnail" style="padding: 20px">
        <div class="caption">
            <div class="panel-body">
                <div class="login-panel panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Wijzig gegevens</h3>
                    </div>
                    <script>
                        $(document).ready(function()
                        {
                           $('input[name="cbFoto"]').on('click', function ()
                           {
                               if($(this).prop('checked'))
                               {
                                   $("#foto").hide();
                               }
                               else
                               {
                                   $("#foto").show();
                               }
                               
                           });
                        });
                    </script>
                    <div class="panel-body">
                        <?php
                        $error_msg = $this->session->flashdata('error_msg');
                        if ($error_msg) {
                            echo '<div class="alert alert-danger" role="danger">' . $error_msg . '</div>';
                        }
                        $success_msg = $this->session->flashdata('success_msg');
                        if ($success_msg)
                        {
                            echo '<div class="alert alert-success" role="alert">' . $success_msg . '</div>';
                        }
                        ?>
                        <?php echo form_open_multipart('home/wijzigGegevens'); ?>
                        <!--<form method="post" action="<?php //echo base_url('index.php/user/registerUser'); ?>" enctype="multipart/form-data">-->
                            <div class="form-group">
                                <input class="form-control" placeholder="Gebruikersnaam" name="gebruikersnaam" type="text" required value="<?php echo $gebruikersnaam; ?>">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Email" name="email" type="email" required value="<?php echo $email; ?>">
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="cbFoto" value="behoud">Behoud huidige foto
                            </div>
                            <div class="form-group" id="foto">
                                <label>Selecteer een afbeelding</label>
                                <input class="form-control" name="gebruikerfoto" type="file" id="userfile" accept=".jpg, .jpeg, .png, .gif">
                            </div>
                            <div class="form-group">
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Wijzig gegevens" name="wijzig">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="row justify-content-between text-center">
    <a class="col-md-6 col-sm-12 hero-feature" href="<?php echo base_url('index.php/home');?>"><button type="button" class="btn-primary">Terug</button></a>
    <a class="col-md-6 col-sm-12 hero-feature" href="<?php echo base_url('index.php/user/userLogout');?>">  <button type="button" class="btn-primary">Logout</button></a>
</div>

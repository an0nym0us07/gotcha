<div class="col-md-12 col-sm-12 hero-feature">
    <div class="thumbnail" style="padding: 20px">
        <div class="caption">
            <div class="panel-body">
                <div class="login-panel panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Inloggen</h3>
                    </div>
                    <div class="panel-body">
                        <?php
                        print_r($code);
                        $error_msg_login = $this->session->flashdata('error_msg_login');
                        if ($error_msg_login) {
                            echo '<div class="alert alert-danger" role="danger">' . $error_msg_login . '</div>';
                        }
                        ?>
                        <!--<form method="post" action="<?php echo base_url('index.php/home/setOmschrijving'); ?>">-->
                        <?php echo form_open_multipart('home/setOmschrijving'); ?>
                            <div class="form-group">
                                <input class="form-control" name="gebruikersnaam" type="text" placeholder="gebruikersnaam" requierd>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="wachtwoord" placeholder="wachtwoord" required>
                            </div>
                            <input type="hidden" name="code" value="<?php echo $code;?>"/>
                            <div class="form-group">
                                <textarea type=text" name="omschrijving" rows="5" placeholder="Beschrijving moord"></textarea>
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Login" name="login">
                        </form>
                        <?php
                            $error_vermoord_msg = $this->session->flashdata('error_vermoord_msg');
                            if($error_vermoord_msg)
                            {
                                echo '<div class="alert alert-danger" role="danger">' . $error_vermoord_msg . '</div>';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

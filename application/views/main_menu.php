<div class="col-md-12 col-sm-12 hero-feature">
    <div class="thumbnail" style="padding: 20px">
        <div class="caption">
            <h3 id="login">Welkom <?php echo $_SESSION['gebruikersnaam']; ?></h3>
            <h4>Je hebt <?php echo $_SESSION['aantalKills'];?> kills!</h4>
        </div>
    </div>
</div>
<?php
    if(isset($_SESSION['slachtofferGebruikersnaam']) && $_SESSION['levend'])
    {
        $this->load->view('target');
    }
    else if(!$_SESSION['slachtofferGebruikersnaam'])
    {
        echo '<h2 id="timer2"></h2>'; 
    }
    else
    {
        echo "<h2>Je bent vermoord</h2>";
    }
    
?>
<script>
    var countDownDate2 = new Date("Aug 18, 2018 00:00:00").getTime();
    
    var x = setInterval(function()
    {
       var now = new Date().getTime();
       
       var distance2 = countDownDate2 - now;
       
       var days = Math.floor(distance2 / (1000 * 60 * 60 * 24));
       var hours = Math.floor((distance2 % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
       var minutes = Math.floor((distance2 % (1000 * 60 * 60)) / (1000 * 60));
       var seconds = Math.floor((distance2 % (1000 * 60)) / 1000);
       
       document.getElementById("timer2").innerHTML = "Over " + days + "d " + hours + "h " + minutes + "m " + seconds + "s worden de slachtoffers bekend gemaakt!";
       
    }, 1000);
</script>
<div class="row">
    <a class="col-md-4 col-sm-12 hero-feature" href="<?php echo base_url('index.php/home/toonSpelers');?>" ><button type="button" class="btn-primary">Toon alle spelers</button></a>
    <a class="col-md-4 col-sm-12 hero-feature" href="<?php echo base_url('index.php/home/toonRegels');?>"><button type="button" class="btn-primary">Toon regels</button></a>
    <a class="col-md-4 col-sm-12 hero-feature" href="<?php echo base_url('index.php/user/userLogout');?>"><button type="button" class="btn-primary">Logout</button></a>
</div>

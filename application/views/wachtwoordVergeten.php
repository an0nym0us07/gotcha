<?php
    $error_msg = $this->session->flashdata('error_msg');
    if ($error_msg) {
        echo '<div class="alert alert-danger" role="danger">' . $error_msg . '</div>';
    }
    $success_msg = $this->session->flashdata('success_msg');
    if ($success_msg)
    {
        echo '<div class="alert alert-success" role="alert">' . $success_msg . '</div>';
    }
?>
<?php echo form_open_multipart('user/wijzigWachtwoord');?>
    <div class="form-group">
        <input class="form-control" placeholder="Gebruikersnaam" name="gebruikersnaam" type="text" required>
    </div>
    <div class="form-group">
        <input class="form-control" placeholder="Email" name="email" type="email" required>
    </div>
    <div class="form-group">
        <input class="form-control" placeholder="Wachtwoord" name="wachtwoord" type="password" value="" required>
    </div>
    <div class="form-group">
        <input class="form-control" placeholder="Herhaal wachtwoord" name="herhaalWachtwoord" type="password" value="" required>
    </div>
    <div class="form-group">
    <input class="btn btn-lg btn-success btn-block" type="submit" value="Wijzig wachtwoord" name="wijzig">
    </div>
</form>

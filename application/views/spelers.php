<h2>Actieve spelers</h2>
<?php
    echo '<div class="w3-row-padding w3-margin-top">';
    foreach ($spelers as $speler)
    {
        echo '<div class="card col-md-4 col-sm-12">';
        echo '<span width="200px" height="200px">';
        echo toonAfbeelding($speler->foto, 'height = 200px', 'card-img-top');
        echo '</span>';
        echo '<div class="card-body">';
        echo '<p class="card-text">' . $speler->gebruikersnaam . '</br> ';
        if($speler->id == $_SESSION['id'])
        {
            echo '<a href="' . base_url('index.php/home/wijzigPagina') . '">Wijzig gegevens</a> </br>';
        }
        
        $moorden = $this->moord_model->getMoorden($speler->id);
        if(empty($moorden))
        {
            echo "<span><b>Nog geen moorden gepleegd!</b></span></p>";
        }
        else
        {
            echo "<span><b>Gepleegde moorden:</b></span>";
            echo "<ol>";
            foreach ($moorden as $moord)
            {
                echo "<li>" . $moord['omschrijving'] . "</li>";
            }
            echo "</ol></p>"; 
        }
        
        echo '</div></div>';
    }
?>
</div>
<div class="row justify-content-between text-center">
    <a class="col-md-6 col-sm-12 hero-feature" href="<?php echo base_url('index.php/home');?>"><button type="button" class="btn-primary">Terug</button></a>
    <a class="col-md-6 col-sm-12 hero-feature" href="<?php echo base_url('index.php/user/userLogout');?>">  <button type="button" class="btn-primary">Logout</button></a>
</div>

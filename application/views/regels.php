<h1>Gotcha: de spelregels</h1>
<h2>Belangrijke data</h2>
<p>De eerste moord mag gepleegd worden op <b>18 augustus vanaf 00:00 uur</b>.</p>
<p>Een winnaar zou gekend moeten zijn op <b>31 december 00:00 uur</b>.</p>
<h2>Do's ofwel de dingen die wel toegestaan zijn</h2>
<p>Als je jezelf als deelnemer geregistreerd hebt zal je terecht komen op de website. Je krijgt op deze website alle informatie voor de speelpleingotcha en ook jouw slachtoffer.</p>
<p>Een geldige moord pleeg je door deze persoon nat te maken met een waterpistool of eender welk ander waterhoudend medium. Van deze actie mag echter niemand buiten je slachtoffer getuigen zijn (de getuigen moet het natmaken zelf niet hebben gezien). Vanaf er iemand in de buurt is die u kan zien, ook al moet hij zich daar voor omdraaien mag er niet gemoord worden. Heeft toch iemand de moord kunnen zien gebeuren <b>(wees hier eerlijk in!)</b> teld deze niet maar behoudt de moordenaar hetzelfde slachtoofer. Je zorgt er dus best voor dat dit niet gebeurd!</p>
<p>Bij een correcte moordpoging is het slachtoffer verplicht om aan te geven op de website dat deze vermoord is. Dan krijg je een QR code of een code. Deze code geef je aan u moordenaar. Met deze code krijgt de moordenaar een nieuw slachtoffer.</p>
<p><b>Belangrijke omperking</b> hierbij is dat als je heel duidelijk vermood bent (bijvoorbeeld samen in één WChokje met het waterpistool op u bakkes gericht maar het werkt niet) maar om de een of andere reden het watermedium van je tegenstander niet werkt, geldt dit toch als een geslaagde moord. Dis is om onnodig tuffen op elkaar te vermijden.</p>
<h2>Dont's ofwel de dingen die absoluut NIET toegestaan zijn</h2>
<p>Tussen de verschillende spelers mag er geen vrij verkeer van slachtoffers ontstaan wand de website heeft dit door. Je kan enkel je eigen slachtoffer vermoorden ookal heb je de code van iemand anders! Je slachtoffer blijft je slachtoffer tot je het hebt omgelegd en dus een ander slachtoffer krijgt.</p>
<p>Je mag best uitdagende en innovatieve dingen doen maar het is ten strengste verboden om wetten te overtreden. Mocht de politie betrokken worden dan "I know from nothing..."</p>
<p>Mocht er een discussie ontstaan tussen slachtoffer en moordenaar wordt deze twist opgelost door de jury (Lukas Jansen) die trouwens altijd gelijk heeft! <small>Paulien haar woorden :)</small></p>
<p>Tijdens speelpleinactiviteiten die direct in functie staan van het welzijn van het speelplein is het verboden te spelen. we hebben het dan over:
<ul>
    <li>Tijdens speelpleindagen van 8 tot 19 uur of tot het opkuisliedje speelt (welk van de 2 het laatste plaats vindt)</li>
    <li>Tijdens de opkuis van 8 tot het kampvuur wordt aangestoken</li>
    <li>Tijdens tiener overnachtingen en tienerwinkel (enkel gelding voor mensen aanwezig of desbetreffende activiteit)</li>
    <li>Tijdens kerngroepen</li>
    <li>Tijdens werkgroepen</li>
</ul>
</p>
<h2>Varia</h2>
<p>Je kan winnen in drie categorieën:
<ul>
    <li>Laatste overlevende en dus overwinnaar</li>
    <li>Meeste kills op jouw naam</li>
    <li>Meest spectaculaire/originele moord</li>
</ul>
</p>
<p><b>!Wees eerlijk en sportief! Maar geniet zeker ook van het spel met al zijn uitdagingen.</b></p>

<div class="row justify-content-between text-center">
    <?php
        if(isset($_SESSION['gebruikersnaam']) && !$_SESSION['regelsGoedgekeurd'])
        {
            echo '<a class="col-md-12 col-sm-12 hero-feature" href=' . base_url("index.php/home/keurGoed") . '><button type="button" class="btn-primary">Accepteer</button></a>';
        }
        else if(isset ($_SESSION['gebruikersnaam']))
        {
            echo '<a class="col-md-12 col-sm-12 hero-feature" href=' . base_url("index.php/home") . '><button type="button" class="btn-primary">Terug</button></a>';
        }
        else
        {
            echo '<a class="col-md-12 col-sm-12 hero-feature" href=' . base_url("index.php/user") . '><button type="button" class="btn-primary">Terug</button></a>';
        }
    ?>
</div>

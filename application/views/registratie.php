<div class="col-md-6 col-sm-12 hero-feature">
    <div class="thumbnail" style="padding: 20px">
        <div class="caption">
            <div class="panel-body">
                <div class="login-panel panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Registratie</h3>
                    </div>
                    <div class="panel-body">

                        <?php
                        $error_msg = $this->session->flashdata('error_msg');
                        if ($error_msg) {
                            echo '<div class="alert alert-danger" role="danger">' . $error_msg . '</div>';
                        }
                        $success_msg = $this->session->flashdata('success_msg');
                        if ($success_msg)
                        {
                            echo '<div class="alert alert-success" role="alert">' . $success_msg . '</div>';
                        }
                        ?>
                        <?php echo form_open_multipart('user/registerUser'); ?>
                        <!--<form method="post" action="<?php //echo base_url('index.php/user/registerUser'); ?>" enctype="multipart/form-data">-->
                            <div class="form-group">
                                <input class="form-control" placeholder="Gebruikersnaam" name="gebruikersnaam" type="text" required>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Email" name="email" type="email" required>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Wachtwoord" name="wachtwoord" type="password" value="" required>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Herhaal wachtwoord" name="herhaalWachtwoord" type="password" value="" required>
                            </div>
                            <div class="form-group">
                                <label>Selecteer een afbeelding</label>
                                <input class="form-control" name="userfile" type="file" id="userfile" accept=".jpg, .jpeg, .png, .gif">
                            </div>
                            <div class="form-group">
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Registreer" name="registreer">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
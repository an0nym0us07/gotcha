<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_model
 *
 * @author Lukas Jansen
 */
class Moord_model extends CI_Model{
    //put your code here
    function setOmschrijving($moord)
    {
        $this->db->insert('moorden', $moord);
    }
    
    function getMoorden($spelerId)
    {
        $this->db->from('moorden');
        $this->db->select('*');
        $this->db->where('spelerId', $spelerId);
        $query = $this->db->get();
        return $query->result_array();
    }
}



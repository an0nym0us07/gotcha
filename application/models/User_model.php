<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_model
 *
 * @author Lukas Jansen
 */
class User_model extends CI_Model{
    //put your code here
    function registerUser($user)
    {
        $this->db->insert('spelers', $user);
    }
    
    function loginUser($gebruikersnaam, $wachtwoord)
    {
        $this->db->select('*');
        $this->db->from('spelers');
        $this->db->where('gebruikersnaam', $gebruikersnaam);
        $query = $this->db->get();
        if($query->num_rows() == 1)
        {
            $gebruiker = $query->row_array();
            if(password_verify($wachtwoord, $gebruiker['wachtwoord']))
            {
                return $gebruiker;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;;
        }
    }
    
    function gebruikersnaamCheck($gebruikersnaam)
    {
        $this->db->select('*');
        $this->db->from('spelers');
        $this->db->where('gebruikersnaam', $gebruikersnaam);
        $query = $this->db->get();
        
        if($query->num_rows()==0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    function getSpelers()
    {
        $this->db->select('*');
        $this->db->from('spelers');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function setSlachtoffer($spelerId, $slachtofferId)
    {
        $this->db->from('spelers');
        $this->db->where('id', $spelerId);
        $this->db->set('slachtofferId', $slachtofferId);
        $this->db->update('spelers');
    }
    
    function getSlachtoffer($slachtofferId)
    {
        $this->db->from('spelers');
        $this->db->select('*');
        $this->db->where('id', $slachtofferId);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function setUniekeCode($id)
    {
        $this->db->from('spelers');
        $this->db->where('id', $id);
        $this->db->set('uniekecode', uniqid());
        $this->db->update('spelers');
    }
    
    function getUniekeCode($id)
    {
        $this->db->select('uniekecode');
        $this->db->where('id', $id);
        $query = $this->db->get('spelers');
        return $query->row('uniekecode');
    }
    
    function vermoord($id)
    {
        $this->db->from('spelers');
        $this->db->where('id', $id);
        $this->db->set('levend', 0);
        $this->db->update('spelers');
    }
    
    function getAllSpelers()
    {
        $this->db->from('spelers');
        $this->db->order_by('gebruikersnaam');
        $this->db->select('*');
        $query = $this->db->get();
        return $query->result();
    }
    
    function getAantalKills($spelerId)
    {
        $this->db->from('spelers');
        $this->db->select('aantalKills');
        $this->db->where('id', $spelerId);
        $query = $this->db->get();
        return $query->row('aantalKills');
    }
    
    function setAantalKills($spelerId, $aantalKills)
    {
        $this->db->from('spelers');
        $this->db->where('id', $spelerId);
        $this->db->set('aantalKills', $aantalKills);
        $this->db->update('spelers');
    }
    
    function slachtofferIsSet($slachtofferId)
    {
        $this->db->from('spelers');
        $this->db->where('slachtofferId', $slachtofferId);
        $query = $this->db->get();
        if($query->num_rows()==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    function keurGoed($id)
    {
        $this->db->from('spelers');
        $this->db->where('id', $id);
        $this->db->set('regelsGoedgekeurd', 1);
        $this->db->update('spelers');
    }
    
    function wijzigUser($user)
    {
        $this->db->from('spelers');
        $this->db->where('id', $user['id']);
        $this->db->set('gebruikersnaam', $user['gebruikersnaam']);
        $this->db->set('email', $user['email']);
        $this->db->set('foto', $user['foto']);
        $this->db->update('spelers');
    }
    
    function wijzigWachtwoord($gebruikersnaam, $wachtwoord)
    {
        $this->db->from('spelers');
        $this->db->where('gebruikersnaam', $gebruikersnaam);
        $this->db->set('wachtwoord', $wachtwoord);
        $this->db->update('spelers');
    }    
}
